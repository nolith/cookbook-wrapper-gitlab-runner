name             'cookbook-wrapper-gitlab-runner'
maintainer       'GitLab Inc.'
maintainer_email 'marin@gitlab.com'
license          'MIT'
description      'Wrapper for cookbook-gitlab-runner'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.11'

depends 'gitlab-vault'
depends 'cookbook-gitlab-runner'

