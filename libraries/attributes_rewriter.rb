class AttributesRewriter
  def initialize(node)
    @node = node
    @attributes = @node.default['cookbook-gitlab-runner']
  end

  def rewrite!
    prepare!
    @node.default['cookbook-gitlab-runner'] = @attributes
    @node
  end

  private

  def prepare!
    runners.each do |name, runner|
      runners[name] = prepare_runner(name, runner)
    end

    @attributes['runners'] = runners
  end

  def prepare_runner(name, runner)
    runner['global'] = prepare_runner_global(name, runner['global'])
    runner['machine'] = prepare_runner_machine(name, runner['machine']) if runner.key?('machine')
    runner
  end

  def prepare_runner_global(name, global)
    global['name'] = "#{host_slug}-#{global['name']}"
    return global unless !global['token']

    global['token'] = host_runner_token(name)
    global
  end

  def prepare_runner_machine(name, machine)
    machine['MachineName'] = "#{host_slug}-#{machine['MachineName']}"
    machine['MachineOptions'].map! do |option|
      if option.match /^digitalocean-access-token=$/
        "digitalocean-access-token=#{host_runner_digitalocean_token(name)}"
      else
        option
      end
    end
    machine
  end

  def host_runner_token(runner)
    if runner_tokens.has_key?(host) && runner_tokens[host].has_key?(runner)
      return runner_tokens[host][runner]
    end

    nil
  end

  def host_runner_digitalocean_token(runner)
    if digitalocean_tokens.has_key?(host) && digitalocean_tokens[host].has_key?(runner)
      return digitalocean_tokens[host][runner]
    end

    nil
  end

  def runners
    @runners ||= @attributes['runners'] || {}
  end

  def runner_tokens
    @runner_tokens ||= @attributes['runner_tokens'] || {}
  end

  def digitalocean_tokens
    @digitalocean_tokens ||= @attributes['digitalocean_tokens'] || {}
  end

  def host
    @host ||= @node.name
  end

  def host_slug
    @host_slug ||= @node.name.gsub(/[^a-zA-Z0-9]/, '-')
  end
end
