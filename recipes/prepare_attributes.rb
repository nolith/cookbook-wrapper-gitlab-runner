include_recipe 'gitlab-vault'
node.default['cookbook-gitlab-runner'] = GitLab::Vault.get(node, 'cookbook-gitlab-runner')

cookbook_wrapper_gitlab_runner = GitLab::Vault.get(node, 'cookbook-wrapper-gitlab-runner')
if cookbook_wrapper_gitlab_runner['use_rewriter']
  rewriter = AttributesRewriter.new(node)
  node = rewriter.rewrite!
end
