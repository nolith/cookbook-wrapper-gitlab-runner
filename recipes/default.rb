#
# Cookbook Name:: cookbook-wrapper-gitlab-runner
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#

include_recipe '::prepare_attributes'
include_recipe 'cookbook-gitlab-runner::default'
include_recipe '::op_scripts'
