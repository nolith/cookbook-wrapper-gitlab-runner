cookbook_file '/root/machines_operations.sh' do
  owner 'root'
  group 'root'
  mode '0755'
end

# Needed to runner_upgrade.sh script on upstart
cookbook_file '/etc/init/gitlab-runner.override' do
  owner 'root'
  group 'root'
  mode '0755'
end

# Needed to runner_upgrade.sh script on systemd
directory '/etc/systemd/system/gitlab-runner.service.d' do
  owner 'root'
  group 'root'
  mode '0755'
  recursive true
end

cookbook_file '/etc/systemd/system/gitlab-runner.service.d/kill.conf' do
	owner 'root'
  group 'root'
  mode '0644'
end

cookbook_file '/root/runner_upgrade.sh' do
  owner 'root'
  group 'root'
  mode '0755'
end
