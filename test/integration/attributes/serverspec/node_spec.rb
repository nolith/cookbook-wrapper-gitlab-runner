require 'json'
require 'serverspec'

# Required by serverspec
set :backend, :exec

describe 'attributes' do
  let(:hostname) { (JSON.parse(IO.read('/tmp/kitchen_chef_node.json')))['name'] }
  let(:data) { (JSON.parse(IO.read('/tmp/kitchen_chef_node.json')))['data'] }
  let(:runner_1) { data['runners']['runner_1'] }
  let(:runner_2) { data['runners']['runner_2'] }
  let(:runner_3) { data['runners']['runner_3'] }

  it { expect(runner_1['global']['name']).to match /#{hostname}-/ }
  it { expect(runner_1['global']['token']).to match /#{hostname}-TOKEN-\d+/ }
  it { expect(runner_1['machine']['MachineName']).to match /#{hostname}-/ }
  it 'has valid digitalocean-access-token' do
    runner_1['machine']['MachineOptions'].each do |option|
      if option.match /digitalocean-access-token=/
        expect(option).to match /#{hostname}-DO-TOKEN-\d+/
      end
    end
  end

  it { expect(runner_2['global']['name']).to match /#{hostname}-/ }
  it { expect(runner_2['global']['token']).to eq 'EXISTING_TOKEN' }
  it { expect(runner_2['machine']['MachineName']).to match /#{hostname}-/ }
  it 'has valid digitalocean-access-token' do
    runner_2['machine']['MachineOptions'].each do |option|
      if option.match /digitalocean-access-token=/
        expect(option).to eq 'digitalocean-access-token=EXISTING_DO_TOKEN'
      end
    end
  end

  it { expect(runner_3['global']['name']).to match /#{hostname}-/ }
  it { expect(runner_3['global']['token']).to match /#{hostname}-TOKEN-\d+/ }
  it { expect(runner_3).not_to have_key('machine') }
end
