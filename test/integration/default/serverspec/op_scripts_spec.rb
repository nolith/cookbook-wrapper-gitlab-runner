require 'serverspec'

# Required by serverspec
set :backend, :exec

describe 'op_scripts checks' do
  describe file('/root/machines_operations.sh') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 755 }

    its(:content) { should match /__scan_machines/ }
    its(:content) { should match /__list_failing/ }
    its(:content) { should match /remove-failing/ }
  end

  describe file('/root/runner_upgrade.sh') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 755 }

    its(:content) { should match /sudo gitlab-runner stop/ }
    its(:content) { should match /sudo gitlab-runner start/ }
    its(:content) { should match /gitlab-runner --version/ }
  end

  describe file('/etc/systemd/system/gitlab-runner.service.d/kill.conf') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 644 }

    its(:content) { should match /\[Service\]/ }
    its(:content) { should match /TimeoutStopSec=/ }
    its(:content) { should match /KillSignal=/ }
  end
end
