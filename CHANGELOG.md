cookbook-wrapper-gitlab-runner CHANGELOG
========================================

This file is used to list changes made in each version of the cookbook-wrapper-gitlab-runner cookbook.

0.1.0
-----
- Initial release of cookbook-wrapper-gitlab-runner
