#!/bin/bash

set -eo pipefail

if sudo service chef-client status >/dev/null; then
    echo Stopping chef-client...
    sudo service chef-client stop
fi

while sleep 20s; do
    JOBS=$(curl -s http://localhost:9402/debug/jobs/list | wc -l)
    if [[ "$JOBS" == "0" ]]; then
        break
    fi

    echo "$(date --rfc-3339=seconds): ${JOBS} running jobs on ${HOSTNAME}..."
done &

echo Signal stop of build processing...
sudo gitlab-runner stop

if which docker-machine; then
    echo Cleaning up docker-machines...
    sudo -H /root/machines_operations.sh remove-all
fi

echo Starting chef-client once...
sudo chef-client

if ! sudo gitlab-runner status; then
    echo GitLab Runner is not running, starting...
    sudo gitlab-runner start
fi


if ! sudo gitlab-runner status; then
    echo GitLab Runner is not running...
    exit 1
fi

echo Verify the new version...
gitlab-runner --version
