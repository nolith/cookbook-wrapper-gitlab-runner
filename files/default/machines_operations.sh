#!/bin/bash

__scan_machines() {
    for i in ~/.docker/machine/machines/runner-*; do
        id=$(echo $i | egrep -o "[^/]*$")

        echo -n "$id "

        grep "\"DropletID\": 0," $i/config.json 2>&1 >/dev/null
        if [[ "$?" -eq 0 ]]; then
            echo -n "FAILING "
        else
            echo -n "OK      "
        fi

        stat $i/config.json | grep Modify | awk '{print $2"T"$3"Z"$4}'
    done
}

__list_failing() {
    __scan_machines | grep FAILING | awk '{print $1}'
}

__usage() {
    echo -e "$0 usage:\n"

    echo -e "  list           - Print status of all machines"
    echo -e "  count          - Print count of machines in OK/FAILING states"
    echo -e "  list-failing   - List IDs of machines in FAILING state"
    echo -e "  remove-failing - Remove machines in FAILING state"
    echo -e "  remove-all     - Remove all machines"
    echo
}

case "$1" in
    list)
        __scan_machines
        ;;
    count)
        __scan_machines | awk '{print $2}' | sort | uniq -c
        ;;
    list-failing)
        __list_failing
        ;;
    remove-failing)
        __list_failing | xargs -n 1 docker-machine rm -f
        ;;
    remove-all)
        docker-machine ls -q | grep "^runner-" | xargs -r docker-machine rm -f
        ;;
    *)
        __usage
        exit 1
        ;;
esac
